import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Users} from "../../interfaces/user-response";
import {environment} from "../../../environments/environment";
import {Eps} from "../../interfaces/eps-response";
import {Symptom} from "../../interfaces/symptom-response";
import {RegisterRequest} from "../../interfaces/register-request";
import {RegisterResponse} from "../../interfaces/register-response";
import {Genders} from "../../interfaces/gender.interface";

@Injectable({
  providedIn: 'any'
})
export class PointOneService {

  constructor(private http: HttpClient) {
  }

  loadUsersOne() {
    return this.http.get<Users>(environment.api_url + 'api/one/users');
  }

  loadUsersTwo() {
    return this.http.get<Users>(environment.api_url + 'api/one/users');
  }

  loadEps() {
    return this.http.get<Eps>(environment.api_url + 'api/one/eps');
  }

  loadSymptoms() {
    return this.http.get<Symptom>(environment.api_url + 'api/one/symptoms');
  }

  loadGender() {
    return this.http.get<Genders>(environment.api_url + 'api/two/genders');
  }

  register(formData: RegisterRequest) {
    return this.http.post<RegisterResponse>(environment.api_url + 'api/one/punto-1', formData);
  }

  register2(formData: RegisterRequest) {
    return this.http.post<RegisterResponse>(environment.api_url + 'api/two/punto-2', formData);
  }
}
