import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FormStatus} from "../../../models/FormStatus";
import {PointOneService} from "../../../services/one/point.one.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RegisterResponse} from "../../../interfaces/register-response";
import {Genders} from "../../../interfaces/gender.interface";
import {take} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {extractErrorMessagesFromErrorResponse} from "../../../helpers/extractErrorMessagesFromErrorResponse";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  formStatus = new FormStatus();

  constructor(private fb: FormBuilder, private pointOne: PointOneService, private _snackBar: MatSnackBar) {
  }

  openSnackBar(registerResponse: RegisterResponse) {
    this._snackBar.open(registerResponse.data.message);
  }

  public genders: Genders;

  ngOnInit(): void {
    this.getGenders();
    this.initForm();
  }

  private initForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      hemoglobin_level: ['', Validators.required],
      unit_time: ['', Validators.required],
      age: ['', Validators.required],
      gender_id: ['', Validators.required],
    });
  }

  private getGenders() {
    this.pointOne.loadGender().subscribe(resp => {
      this.genders = resp;
    })
  }

  submit() {
    this.formStatus.onFormSubmitting();

    this.pointOne.register2(this.form.value)
      .pipe(
        take(1),
      )
      .subscribe(
        (response) => {
          this.openSnackBar(response)
          this.formStatus.onFormSubmitResponse({success: true, messages: []});
        },
        (errorResponse: HttpErrorResponse) => {
          // Extract form error messages from API  <------ HERE!!!
          const messages = extractErrorMessagesFromErrorResponse(errorResponse);
          this.formStatus.onFormSubmitResponse({success: false, messages: messages});
        },
      );
  }
}
