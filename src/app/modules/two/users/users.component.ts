import {Component, OnInit} from '@angular/core';
import {Users} from "../../../interfaces/user-response";
import {PointOneService} from "../../../services/one/point.one.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  public users: Users;

  constructor(private pointOne: PointOneService) {
  }

  ngOnInit(): void {
    this.pointOne.loadUsersOne().subscribe(resp => {
      this.users = resp;
    })
  }

}
