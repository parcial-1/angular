import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TwoRoutingModule} from './two-routing.module';
import { RegisterComponent } from './register/register.component';
import { UsersComponent } from './users/users.component';
import {MatTableModule} from "@angular/material/table";
import {HttpClientModule} from "@angular/common/http";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatSnackBarModule} from "@angular/material/snack-bar";


@NgModule({
  declarations: [
    RegisterComponent,
    UsersComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    HttpClientModule,
    MatFormFieldModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSnackBarModule,
    TwoRoutingModule
  ]
})
export class TwoModule {
}
