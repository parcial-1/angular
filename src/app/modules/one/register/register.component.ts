import {Component, OnInit} from '@angular/core';
import {PointOneService} from "../../../services/one/point.one.service";
import {Ep} from "../../../interfaces/eps-response";
import {SymptomElement} from "../../../interfaces/symptom-response";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {extractErrorMessagesFromErrorResponse} from "../../../helpers/extractErrorMessagesFromErrorResponse";
import {HttpErrorResponse} from "@angular/common/http";
import {take} from 'rxjs/operators';
import {RegisterRequest} from "../../../interfaces/register-request";
import {FormStatus} from "../../../models/FormStatus";
import {MatSnackBar} from "@angular/material/snack-bar";
import {RegisterResponse} from "../../../interfaces/register-response";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  formStatus = new FormStatus();
  public eps: Ep[] = [];
  public symptoms: SymptomElement[] = [];

  constructor(private fb: FormBuilder, private pointOne: PointOneService, private _snackBar: MatSnackBar) {
  }

  openSnackBar(registerResponse: RegisterResponse) {
    this._snackBar.open(registerResponse.data.message);
  }

  ngOnInit(): void {
    this.initForm();
    this.getEps();
    this.getSymptoms();
  }

  private initForm() {

    this.form = this.fb.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      identification_number: ['', Validators.required],
      eps_id: ['', Validators.required],
      symptom_id: ['', Validators.required],
      blood_glucose_results: ['', Validators.required],
    });

    this.form.get('symptom_id')?.valueChanges
      .subscribe(value => {
          if (value === 4) {
            this.form.get('blood_glucose_results')?.clearValidators();
          } else {
            this.form.get('blood_glucose_results')?.setValidators(Validators.required)
          }
        }
      );
  }

  private getEps() {
    this.pointOne.loadEps().subscribe(resp => {
      this.eps = resp.data.eps;
    })
  }

  private getSymptoms() {
    this.pointOne.loadSymptoms().subscribe(resp => {
      this.symptoms = resp.data.symptoms;
    })
  }

  submit() {
    this.formStatus.onFormSubmitting();

    this.pointOne.register(this.form.value)
      .pipe(
        take(1),
      )
      .subscribe(
        (response) => {
          this.openSnackBar(response)
        },
        (errorResponse: HttpErrorResponse) => {
          // Extract form error messages from API  <------ HERE!!!
          const messages = extractErrorMessagesFromErrorResponse(errorResponse);
          this.formStatus.onFormSubmitResponse({success: false, messages: messages});
        },
      );
  }
}
