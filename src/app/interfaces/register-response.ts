export interface RegisterResponse {
  data: Data;
}

export interface Data {
  message: string;
}
