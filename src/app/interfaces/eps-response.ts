export interface Eps {
  data: Data;
}

export interface Data {
  eps: Ep[];
}

export interface Ep {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
}
