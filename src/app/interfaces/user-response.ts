import {Ep} from "./eps-response";
import {SymptomElement} from "./symptom-response";

export interface Users {
  data: Data;
}

export interface Data {
  users: User[];
}

export interface User {
  id: number;
  name: string;
  lastname: string;
  identification_number: string;
  eps_id: number;
  symptom_id: number;
  created_at: Date;
  updated_at: Date;
  eps: Ep;
  symptom: SymptomElement;
}
