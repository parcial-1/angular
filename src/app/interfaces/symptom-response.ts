export interface Symptom {
  data: Data;
}

export interface Data {
  symptoms: SymptomElement[];
}

export interface SymptomElement {
  id:          number;
  name:        string;
  description?: string;
  created_at:  Date;
  updated_at:  Date;
}
