export interface RegisterRequest {
  name: string;
  lastname: string;
  identification_number: string;
  eps_id: number;
  symptom_id: number;
  blood_glucose_results?: number;
}
