export interface Genders {
  data: Data;
}

export interface Data {
  gender: Gender[];
}

export interface Gender {
  id:         number;
  name:       string;
  created_at: Date;
  updated_at: Date;
}
